//
//  DemoViewController.swift
//  TestCollectionView
//
//  Created by Alex K. on 12/05/16.
//  Copyright © 2016 Alex K. All rights reserved.
//

import UIKit
import Firebase
class DemoViewController: ExpandingViewController {

    typealias ItemInfo = (imageName: String, title: String)
    fileprivate var cellsIsOpen = [Bool]()
    
    @IBOutlet var pageLabel: UILabel!
    var currentuser  = usuarioActual.nickname[0]
    var conjunto = usuarioActual.conjunto[0]
    var visitasAct = usuarioActual.visitas
    var ref = usuarioActual.ref[0]
    
    
  
}

// MARK: - Lifecycle 🌎

extension DemoViewController {

    override func viewDidLoad() {
        itemSize = CGSize(width: 256, height: 460)
        super.viewDidLoad()
        var visitas = usuarioActual.visitas.count
        var visitastot = 0
        var visitasutiles = 0
        let nuevo = visitaSolicitud(key:"",estado:"" ,fecha:"" , hora:"" , nicknameResidente:"No tienes visitas", nicknameVisitante:"No tienes visitas" , Parqueadero:"", Conjunto:"" )
        
        visitasAct.append(nuevo)
        DispatchQueue.global().async {
            self.ref?.child("Visitas").observe(.childAdded , with: { (snap) in
               
                
                let visita = snap.value as? [String : AnyObject] ?? [:]
                //let numparq = snap.key
                let keyt = snap.key
                let est = visita["EstadoVisita"] as? String
                let fec = visita["FechaVisita"] as? String
                let hor = visita["HoraVisita"] as? String
                let nickR = visita["IdRecidente"] as? String
                let nickV = visita["IdVisitante"] as? String
                let park = visita["Parqueadero"] as? String
                let conjunto = visita["Conjunto"] as? String
                
                if((nickV == self.currentuser || nickR == self.currentuser ) && est == "Aceptada")
                {
                    let nuevo = visitaSolicitud(key:keyt,estado:est ,fecha:fec , hora:hor , nicknameResidente:nickR, nicknameVisitante:nickV , Parqueadero:park, Conjunto:conjunto )

                    self.visitasAct.append(nuevo)
                    visitasutiles += 1
                    visitastot += 1
                    if(self.visitasAct.count > 1)
                    {
                        var cont = 0
                        var sigue = true
                        while(cont < self.visitasAct.count && sigue == true )
                        {
                            
                            if(self.visitasAct[cont].nicknameResidente == "No tienes visitas")
                            {
                                self.visitasAct.remove(at: cont)
                                sigue = false
                                self.collectionView?.reloadData()
                            }
                            cont += 1
                        }
                    }
                    DispatchQueue.main.async {
                        self.collectionView?.reloadData()
                    }

                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                    // print(self.visitas.count)
                    //}
                }




            })
        }
        
        DispatchQueue.global().async {
            self.ref?.child("Visitas").observe(.childChanged , with: { (snap) in
                
                let visita = snap.value as? [String : AnyObject] ?? [:]
                //let numparq = snap.key
                let keyt = snap.key
                let est = visita["EstadoVisita"] as? String
                let fec = visita["FechaVisita"] as? String
                let hor = visita["HoraVisita"] as? String
                let nickR = visita["IdRecidente"] as? String
                let nickV = visita["IdVisitante"] as? String
                let park = visita["Parqueadero"] as? String
                let conjunto = visita["Conjunto"] as? String
                
              
                if((nickV == self.currentuser || nickR == self.currentuser ) )
                {
                    if(est == "Aceptada")
                    {
                        let nuevo = visitaSolicitud(key:keyt,estado:est ,fecha:fec , hora:hor , nicknameResidente:nickR, nicknameVisitante:nickV , Parqueadero:park, Conjunto:conjunto )
                        
                        self.visitasAct.append(nuevo)
                        
                        if(self.visitasAct.count > 1)
                        {
                            var cont = 0
                            var sigue = true
                            while(cont < self.visitasAct.count && sigue == true )
                            {
                                
                                if(self.visitasAct[cont].nicknameResidente == "No tienes visitas")
                                {
                                    self.visitasAct.remove(at: cont)
                                    sigue = false
                                    self.collectionView?.reloadData()
                                }
                                cont += 1
                            }
                        }
                        
                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                        }
                        
                    }
                    if(est == "Cancelada")
                    {
                        var cont = 0
                        var sigue = true
                        while(cont < self.visitasAct.count && sigue == true )
                        {
                            
                            if(self.visitasAct[cont].key == keyt)
                            {
                                self.visitasAct.remove(at: cont)
                                sigue = false
                                self.collectionView?.reloadData()
                            }
                            cont += 1
                        }
                        
                    }
                    
                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                    // print(self.visitas.count)
                    //}
                }
                
                
                
                
            })
        }
        if(visitasAct.count > 1)
        {
            var cont = 0
            var sigue = true
            while(cont < self.visitasAct.count && sigue == true )
            {
                
                if(self.visitasAct[cont].nicknameResidente == "No tienes visitas")
                {
                    self.visitasAct.remove(at: cont)
                    sigue = false
                    self.collectionView?.reloadData()
                }
                cont += 1
            }
        }
        registerCell()
        fillCellIsOpenArray()
        addGesture(to: collectionView!)
        configureNavBar()
    }
}

// MARK: Helpers

extension DemoViewController {

    fileprivate func registerCell() {

        let nib = UINib(nibName: String(describing: DemoCollectionViewCell.self), bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: String(describing: DemoCollectionViewCell.self))
    }

    fileprivate func fillCellIsOpenArray() {
        cellsIsOpen = Array(repeating: false, count: visitasAct.count)
    }

//    fileprivate func getViewController() -> ExpandingTableViewController {
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let toViewController = storyboard.instantiateViewController(withIdentifier: "DemoTableViewController")
//        return toViewController as! ExpandingTableViewController
//    }
    
    fileprivate func getViewControllerQR() -> DetailAgendaViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let toViewController = storyboard.instantiateViewController(withIdentifier: "detailQR")
       
        return toViewController as! DetailAgendaViewController
    }

    fileprivate func configureNavBar() {
//        navigationItem.leftBarButtonItem?.image = navigationItem.leftBarButtonItem?.image!.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
    }
}

/// MARK: Gesture
extension DemoViewController {

    fileprivate func addGesture(to view: UIView) {
        let upGesture = Init(UISwipeGestureRecognizer(target: self, action: #selector(DemoViewController.swipeHandler(_:)))) {
            $0.direction = .up
        }

        let downGesture = Init(UISwipeGestureRecognizer(target: self, action: #selector(DemoViewController.swipeHandler(_:)))) {
            $0.direction = .down
        }
        view.addGestureRecognizer(upGesture)
        view.addGestureRecognizer(downGesture)
    }

    @objc func swipeHandler(_ sender: UISwipeGestureRecognizer) {
        let indexPath = IndexPath(row: currentIndex, section: 0)
        guard let cell = collectionView?.cellForItem(at: indexPath) as? DemoCollectionViewCell else { return }
        // double swipe Up transition
//        if cell.isOpened == true && sender.direction == .up {
//            pushToViewController(getViewController())

//            if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
//                rightButton.animationSelected(true)
//            }
        }

//        let open = sender.direction == .up ? true : false
//        cell.cellIsOpen(open)
//        cellsIsOpen[indexPath.row] = cell.isOpened
    
}

// MARK: UIScrollViewDelegate

extension DemoViewController {

    func scrollViewDidScroll(_: UIScrollView) {
       // pageLabel.text = "\(currentIndex + 1)/\(items.count)"
         pageLabel.text = "\(currentIndex + 1)/\(visitasAct.count)"
    }
}

// MARK: UICollectionViewDataSource

extension DemoViewController {

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
        guard let cell = cell as? DemoCollectionViewCell else { return }

//        let index = indexPath.row % items.count
//        let info = items[index]
//        cell.backgroundImageView?.image = UIImage(named: info.imageName)
//        cell.customTitle.text = info.title
       // print(visitasAct)
        
            let index = indexPath.row % self.visitasAct.count
            let info = self.visitasAct[indexPath.row]
            cell.backgroundImageView?.image = UIImage(named: "avatar")
            cell.customTitle.text = info.hora + "   -   " + info.nicknameVisitante
            cell.customTitle.textColor = UIColor.black
        
        // cuando soy recidente
        
        if (info.nicknameResidente == currentuser)
            {
                cell.backgroundColor = #colorLiteral(red: 1, green: 0.7307798266, blue: 0.7761104703, alpha: 1)
                cell.customTitle.text = info.hora + "   -   " + info.nicknameVisitante
            }
        
        // Cuando visito
        
            if(info.nicknameVisitante == currentuser)
            {
                
                cell.backgroundColor  = #colorLiteral(red: 0.748375833, green: 1, blue: 0.7562029958, alpha: 1)
                cell.customTitle.text = info.hora + "   -   " + info.nicknameResidente
            }
        
        
            //cell.cellIsOpen(cellsIsOpen[indexPath.row], animated: false)
    
        
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? DemoCollectionViewCell
            , currentIndex == indexPath.row else { return }

        if cell.isOpened == false {
            cell.cellIsOpen(true)
        } else {
           
            if(visitasAct[indexPath.row].nicknameVisitante! == currentuser){
              
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let destion = storyboard.instantiateViewController(withIdentifier: "detailQR") as! DetailAgendaViewController
                let contactoObject = visitasAct[indexPath.row]
                destion.objeto = contactoObject
                var visita = visitasAct[indexPath.row]
                
                
                navigationController?.pushViewController(destion, animated: true)
                
    //            navigationController?.pushViewController(getViewControllerQR(), animated: true)
    //            pushToViewController(getViewControllerQR())
//                if let rightButton = navigationItem.rightBarButtonItem as? AnimatingBarButton {
//                    rightButton.animationSelected(true)
//                }
            }else{
             //   print("no muestra QR")
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let destion = storyboard.instantiateViewController(withIdentifier: "detailsinQR") as! AgendaSinQRViewController
                let contactoObject = visitasAct[indexPath.row]
                destion.objeto = contactoObject
                
                navigationController?.pushViewController(destion, animated: true)
            }
        }
    }
}

// MARK: UICollectionViewDataSource

extension DemoViewController {

    override func collectionView(_: UICollectionView, numberOfItemsInSection _: Int) -> Int {
        return visitasAct.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DemoCollectionViewCell.self), for: indexPath)
    }
}
