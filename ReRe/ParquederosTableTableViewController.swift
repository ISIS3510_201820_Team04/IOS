//
//  ParquederosTableTableViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/25/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class ParquederosTableTableViewController: UITableViewController {
    
    
    var parqueaderos : [parqueaderoPropietario] = []
    var currentuser = usuarioActual.nickname[0]
    var aptoUser = ""
    var conjuntoUser = ""
    var ref: DatabaseReference!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        ref = Database.database().reference()
        
        DispatchQueue.global(qos: .userInteractive).async {
            
        }
        self.ref?.child("Perfiles").child(currentuser).observeSingleEvent(of: .value, with: { (snap) in
            
            let perfil = snap.value as? [String : AnyObject] ?? [:]
            self.aptoUser = (perfil["apto"] as? String)!
            self.conjuntoUser=(perfil["conjunto"] as? String)!
            print(self.aptoUser)
            print(self.conjuntoUser)
            
            self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosPropietarios").observeSingleEvent(of: .value, with:{
                (snap) in
                
                print(snap.debugDescription)
                if  (snap.exists())
                {
                    for rest in snap.children.allObjects as! [DataSnapshot]
                    {
                        let parq = rest.value as? [String : AnyObject] ?? [:]
                        let appa = parq["apto"] as! String
                        let num = rest.key
                        let visitante = parq["Visitante"] as! String
                        let prestamo = parq["prestamo"] as! String
                        let estadoo = parq["Estado"] as! String
                        
                        if(appa == self.aptoUser)
                        {
                            //add to a list
                            print(num)
                            self.parqueaderos.append(parqueaderoPropietario(apto:appa , estado:estadoo, parqueadero:num, prestamo:prestamo , visitante:visitante))
                            
                            self.tableView.reloadData()
                            
                        }
                    }
                    
                }
                
                
                
            })

        })
        
        
        self.ref?.child("Perfiles").child(currentuser).observeSingleEvent(of: .value, with: { (snap) in
            
            let perfil = snap.value as? [String : AnyObject] ?? [:]
            self.aptoUser = (perfil["apto"] as? String)!
            self.conjuntoUser=(perfil["conjunto"] as? String)!
            print(self.aptoUser)
            print(self.conjuntoUser)
            
            self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosPropietarios").observe(.childChanged, with: { (snap) in
                print(snap.debugDescription)
                
                
                    let parq = snap.value as? [String : AnyObject] ?? [:]
                    let appa = parq["apto"] as! String
                    let num = snap.key
                    let visitante = parq["Visitante"] as! String
                    let prestamo = parq["prestamo"] as! String
                    let estadoo = parq["Estado"] as! String
                    
                    if(appa == self.aptoUser)
                    {
                        var salir = false
                        var a = 0
                        //add to a list
                        while(salir == false && a < self.parqueaderos.count)
                        {
                            if(self.parqueaderos[a].parqueadero == num)
                            {
                                self.parqueaderos.remove(at:a)
                                salir = true
                            }
                            a = a+1
                        }
                        print(num)
                        self.parqueaderos.append(parqueaderoPropietario(apto:appa , estado:estadoo, parqueadero:num, prestamo:prestamo , visitante:visitante))
                        
                        self.tableView.reloadData()
                        
                    }
                
                
            })
            
                
                
           
            
        })
    
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
            return parqueaderos.count
        
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Privado", for: indexPath) as! ParqueaderoTableViewCell
        
        
            let vehi = parqueaderos[indexPath.row]
            cell.nuemeroPark.text = vehi.parqueadero
            cell.estadoPark.text = vehi.estado
            if(vehi.prestamo == "true")
            {
                cell.backgroundColor = #colorLiteral(red: 0.748375833, green: 1, blue: 0.7562029958, alpha: 1)
        }
            else{
                cell.backgroundColor = #colorLiteral(red: 1, green: 0.7307798266, blue: 0.7761104703, alpha: 1)
        }

        return cell
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
       
        
        
        let lock = UITableViewRowAction(style: .normal, title: "Bloquear") { action, index in
            
            let cell = tableView.cellForRow(at: index) as? ParqueaderoTableViewCell
            
            let alerta = UIAlertController(title: "Estas seguro?", message: "Estas bloqueando tu parqueadero", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                //print("You've pressed default");
                var num = (cell!.nuemeroPark.text)
              //  print(cell!.estadoPark.text)
                self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosPropietarios").child(num!).updateChildValues(["prestamo" : "false"])
                
            }
            let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
            alerta.addAction(ok)
            alerta.addAction(cancel)
            self.present(alerta, animated: true, completion: nil)
        }
        lock.backgroundColor = .red
        
        let share = UITableViewRowAction(style: .normal, title: "Compartir") { action, index in
            
            let cell = tableView.cellForRow(at: index) as? ParqueaderoTableViewCell
            
            let alerta = UIAlertController(title: "Estas seguro?", message: "Estas compratiendo tu parqueadero", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
               // print("You've pressed default");
                var num = (cell!.nuemeroPark.text)
                print(cell!.estadoPark.text)
                self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosPropietarios").child(num!).updateChildValues(["prestamo" : "true"])
                
            }
            let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
            alerta.addAction(ok)
            alerta.addAction(cancel)
            self.present(alerta, animated: true, completion: nil)
        }
        share.backgroundColor = .green
        
        return [share, lock]
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
