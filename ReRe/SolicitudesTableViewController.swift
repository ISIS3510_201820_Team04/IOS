//
//  SolicitudesTableViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/25/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class SolicitudesTableViewController: UITableViewController {

    var visitas : [visitaSolicitud] = []
    var currentuser = usuarioActual.nickname[0]
    var conjuntoUser = usuarioActual.conjunto[0]
    var ref: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = Database.database().reference()
        DispatchQueue.global(qos: .userInteractive).async {
            self.ref?.child("Visitas").observe(.childAdded , with: { (snap) in
                let visita = snap.value as? [String : AnyObject] ?? [:]
                //print(visita)
                //let numparq = snap.key
                let keyt = snap.key
                let est = visita["EstadoVisita"] as? String
                let fec = visita["FechaVisita"] as? String
                let hor = visita["HoraVisita"] as? String
                let nickR = visita["IdRecidente"] as? String
                let nickV = visita["IdVisitante"] as? String
                let park = visita["Parqueadero"] as? String
                let conjunto = visita["Conjunto"] as? String
                
                
                if(nickR == self.currentuser && est == "Enviada")
                {
                    let nuevo = visitaSolicitud(key:keyt,estado:est ,fecha:fec , hora:hor , nicknameResidente:nickR, nicknameVisitante:nickV , Parqueadero:park, Conjunto: conjunto)
                    
                    self.visitas.append(nuevo)
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                    // print(self.visitas.count)
                    //}
                }
                
                
                
                
            })

        }
       
        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.visitas.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Solicitud", for: indexPath) as! SolicitudesTableViewCell
        
        
        let visita =  self.visitas[indexPath.row]
        
        cell.code = visita.key
        cell.nombre.text = visita.nicknameVisitante + " quiere visitarte"
        cell.hora.text = "a las " + visita.hora
        if(visita.Parqueadero == "SI")
        {
            cell.parqueadero.text = "Necesita un parqueadero"
        }
        else{
            cell.parqueadero.text = "No necesita parqueadero"
        }// Configure the cell...
        
        return cell
        
        
        

       
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let cancelar = UITableViewRowAction(style: .normal, title: "Cancel") { action, index in
            
            let cell = tableView.cellForRow(at: index) as? SolicitudesTableViewCell
            let alerta = UIAlertController(title: "Estas seguro?", message: "Estas cancelando esta visita", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                print("You've pressed default");
                var key  = (cell!.code)
                
            self.ref?.child("Visitas").child(key).updateChildValues(["EstadoVisita" : "Rechazada"])
                
                self.visitas.remove(at: index.row)
                self.tableView.reloadData()
                
                
                
            }
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alerta.addAction(ok)
            alerta.addAction(cancel)
            self.present(alerta, animated: true, completion: nil)
        }
        cancelar.backgroundColor = .red
        
        let aceptar = UITableViewRowAction(style: .normal, title: "Accept") { action, index in
            
            let cell = tableView.cellForRow(at: index) as? SolicitudesTableViewCell
            
            let alerta = UIAlertController(title: "Estas seguro?", message: "Estas aceptando esta visita", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                //print("You've pressed default");
                var key  = (cell!.code)
                
               
                DispatchQueue.global(qos: .userInitiated).async {
                    self.ref.child("Visitas").child(key).observeSingleEvent(of: .value, with: { (snap) in
                        let visita = snap.value as? [String : AnyObject] ?? [:]
                        var visitante = visita["IdVisitante"] as? String
                        var hora = visita["HoraVisita"] as? String
                        
                        if(visita["Parqueadero"] as? String == "SI")
                        {
                            var necesita = true
                            
                            
                            if(necesita == true)
                            {
                                
                                self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosVisitantes").observeSingleEvent(of: .value, with: { (snap) in
                                    print(snap)
                                    for rest in snap.children.allObjects as! [DataSnapshot]
                                    {
                                        print(rest)
                                        let parq = rest.value as? [String : AnyObject] ?? [:]
                                        if(parq["Estado"] as? String  == "Disponible" && necesita == true)
                                        {
                                            self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosVisitantes").child(rest.key).updateChildValues(["Estado" : "Reservado", "horaingreso": hora, "Visitante": visitante, "apto": usuarioActual.apto[0]])
                                            necesita = false
                                            
                                        }}
                                })
                            }
                            if(necesita == true)
                            {
                                
                                self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosPropietarios").observeSingleEvent(of: .value, with: { (snap) in
                                    print(snap)
                                    for rest in snap.children.allObjects as! [DataSnapshot]
                                    {
                                        print(rest)
                                        let parq = rest.value as? [String : AnyObject] ?? [:]
                                        if(parq["Estado"] as? String  == "Disponible" && necesita == true && parq["prestamo"] as? String == "true")
                                        {
                                            self.ref?.child("Conjuntos").child(self.conjuntoUser).child("ParqueaderosPropietarios").child(rest.key).updateChildValues(["Estado" : "Reservado", "Visitante": visitante])
                                            necesita = false
                                            
                                        }}
                                })
                            }
                        }
                        
                    })
                    self.ref?.child("Visitas").child(key).updateChildValues(["EstadoVisita" : "Aceptada"])
                    
                    self.visitas.remove(at: index.row)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                
                }
            }
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alerta.addAction(ok)
            alerta.addAction(cancel)
            self.present(alerta, animated: true, completion: nil)
        }
        aceptar.backgroundColor = .green
        
        return [aceptar, cancelar]
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
