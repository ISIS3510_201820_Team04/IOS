//
//  BusquedasCeladorTableViewCell.swift
//  ReRe
//
//  Created by Julian Camilo on 25/09/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

class BusquedasCeladorTableViewCell: UITableViewCell {

    @IBOutlet weak var displayplaca: UILabel!
    @IBOutlet weak var displayduenio: UILabel!
    @IBOutlet weak var displayApto: UILabel!
    @IBOutlet weak var displayPark: UILabel!
    @IBOutlet weak var carroImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
