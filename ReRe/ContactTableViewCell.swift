//
//  ContactTableViewCell.swift
//  ReRe
//
//  Created by Julian Camilo on 9/20/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var item: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
