//
//  QRViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 11/5/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//
import AVFoundation
import UIKit
import Firebase

class QRViewController: UIViewController, QRCodeReaderViewControllerDelegate {
    
   
    
    @IBOutlet weak var nombreconjunto: UILabel!
    
    @IBOutlet weak var disponiblesVisitantes: UILabel!
    @IBOutlet weak var ocupadosVisitantes: UILabel!
    @IBOutlet weak var disponiblesResidentes: UILabel!
    @IBOutlet weak var ocupadosResidentes: UILabel!
    var dV=0
    var oV=0
    var dR=0
    var oR=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nombreconjunto.text = usuarioActual.conjunto[0]
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        ref?.child("Conjuntos").child(usuarioActual.conjunto[0]).child("ParqueaderosPropietarios").observe(.childAdded, with: { (snap) in
            
            let parkP = snap.value as? [String : AnyObject] ?? [:]
           
            let estadoP = (parkP["Estado"] as? String)!
            let prestamoP = (parkP["prestamo"] as? String)!
            if( estadoP == "Disponible" ){
                self.dR = self.dR+1
            }else{
                self.oR = self.oR+1
            }
            self.disponiblesResidentes.text = String(self.dR)
            self.ocupadosResidentes.text = String(self.oR)
        
        })
        ref?.child("Conjuntos").child(usuarioActual.conjunto[0]).child("ParqueaderosVisitantes").observe(.childAdded, with: { (snap) in
            let parkV = snap.value as? [String : AnyObject] ?? [:]
            let estadoV = (parkV["Estado"] as? String)
            if(estadoV == "Disponible"){
                self.dV = self.dV+1
            }else{
                self.oV = self.oV+1
            }
            
            self.disponiblesVisitantes.text = String(self.dV)
            self.ocupadosVisitantes.text = String(self.oV)
            
        })
        
        
       
        
  
        
        
    }
    @IBOutlet weak var previewView: QRCodeReaderView! {
        didSet {
            previewView.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: true, reader: reader)
        }
    }
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    // MARK: - Actions
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    @IBAction func scanInModalAction(_ sender: AnyObject) {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
    }
    

    
    // MARK: - QRCodeReader Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        let rta =  (result.value).split(separator: ",")
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "Autorizar esta visita?",
                message:"",
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
                self!.autorizarVisita(tipo: String(rta[0]), parqueadero : String(rta[1]), visitante: String(rta[2]),tipoP: String(rta[3]),conjunto: String(rta[4]))
            }
            ))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func autorizarVisita(tipo: String ,parqueadero: String, visitante: String, tipoP : String, conjunto : String ){
        var ref: DatabaseReference!
        ref = Database.database().reference()
        if(tipo == "entrada"){
            ref.child("Conjuntos").child(conjunto).child(tipoP).child(parqueadero).updateChildValues( ["Estado":"Ocupado","Visitante":visitante ])
            
        }
        if(tipo == "salida"){
            ref.child("Conjuntos").child(conjunto).child(tipoP).child(parqueadero).updateChildValues( ["Estado":"Disponible","Visitante":"Libre" ])
        }
         print(tipo, conjunto, tipoP, parqueadero)
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
}
