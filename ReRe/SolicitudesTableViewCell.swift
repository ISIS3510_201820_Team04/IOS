//
//  SolicitudesTableViewCell.swift
//  ReRe
//
//  Created by Julian Camilo on 10/25/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

class SolicitudesTableViewCell: UITableViewCell {

    var code = ""
    
    @IBOutlet weak var parqueadero: UILabel!
    @IBOutlet weak var hora: UILabel!
    @IBOutlet weak var nombre: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
