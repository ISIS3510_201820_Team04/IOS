//
//  AgendaSinQRViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 12/10/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class AgendaSinQRViewController: UIViewController {
    
 var objeto = visitaSolicitud()
    @IBOutlet weak var parqueadero: UITextField!
    @IBOutlet weak var estado: UITextField!
    @IBOutlet weak var fecha: UITextField!
    @IBOutlet weak var visitante: UITextField!
    @IBOutlet weak var hora: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.parqueadero.text = objeto.Parqueadero + " necesita parqueadero"
        self.visitante.text = objeto.nicknameVisitante
        self.estado.text = objeto.estado
        self.fecha.text = objeto.fecha
        self.hora.text = objeto.hora
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelarVisita(_ sender: Any)
    {
        print("Cancelar")
        var key = objeto.key
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        let alerta = UIAlertController(title: "Estas seguro?", message: "Estas cancelando esta visita", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            print("You've pressed default");
            
            if(self.objeto.estado == "" || self.objeto.key == "")
            {
                self.performSegue(withIdentifier: "aMain", sender: self)
            }
            else{
                ref?.child("Visitas").child(key!).updateChildValues(["EstadoVisita" : "Cancelada"])
                
                self.performSegue(withIdentifier: "aMain", sender: self)
            }
            
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alerta.addAction(ok)
        alerta.addAction(cancel)
        self.present(alerta, animated: true, completion: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
