//
//  conjuntoCeladorViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 11/5/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class conjuntoCeladorViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var conjuntoPicker: UIPickerView!
    var conjuntos : [String] = []
     var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        ref?.child("Conjuntos").observe(.childAdded, with: { (snapshot) in
            // let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            let con = snapshot.key
            self.conjuntos.append(con)
            self.conjuntoPicker.dataSource = self
            self.conjuntoPicker.delegate = self
        })
        // Do any additional setup after loading the view.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return conjuntos.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return conjuntos[row]
    }
    
    @IBAction func slectConjunto(_ sender: Any) {
        
        let conjunteishion = conjuntoPicker.selectedRow(inComponent: 0)
        let conjuntoa = conjuntos[conjunteishion]
        usuarioActual.conjunto.append(conjuntoa)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
