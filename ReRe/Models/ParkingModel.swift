//
//  ParkingModel.swift
//  ReRe
//
//  Created by Diego  Rodriguez on 9/27/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import Foundation

struct parqueadero
{
    var placa: String!
    var marca:String!
    var apartamento: String!
    var visitante: String!
    var parqueadero : String!
    var foto:String!
    var estado : String!
    var horaIngreso : String!
    
}

struct parqueaderoPropietario {
    var apto : String!
    var estado : String!
    var parqueadero : String!
    var prestamo : String!
    var visitante : String!
    
}
