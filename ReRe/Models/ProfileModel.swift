//
//  ProfileModel.swift
//  ReRe
//
//  Created by Diego  Rodriguez on 9/21/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import Foundation
import Firebase
struct friend
{
    var name: String!
    var item: String!
    var image: String!
    var nickname: String!
}

struct visitaPersona
{
    var estado: String!
    var fecha: String!
    var hora: String!
    var nicknameResidente: String!
    var nicknameVisitante: String!
    var Parqueadero: String!
    var Conjunto: String!
}

struct visitaSolicitud
{
    var key : String!
    var estado: String!
    var fecha: String!
    var hora: String!
    var nicknameResidente: String!
    var nicknameVisitante: String!
    var Parqueadero: String!
    var Conjunto: String!
}

struct usuarioActual{
    static var nickname = [String]()
    static var conjunto = [String]()
    static var apto = [String]()
    static var visitas = [visitaSolicitud]()
    static var ref = [DatabaseReference?] ()
}
