//
//  TimersCollectionViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 5/10/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import SystemConfiguration

private let reuseIdentifier = "Timer"

class TimersCollectionViewController: UICollectionViewController , UICollectionViewDelegateFlowLayout{

     var lots : [parqueadero] = []
    var estimateWidth = 160.0
    var cellMarginSize = 16.0
    @IBOutlet var grid: UICollectionView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        alertaInternet()
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        // Register cells
        self.grid.register(UINib(nibName: "TimersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        // SetupGrid view
        self.setupGridView()

       ///*
        
        DispatchQueue.global(qos: .userInteractive).async {
            ref?.child("Conjuntos").child("Torres de la colina").child("ParqueaderosVisitantes").observe(.childAdded , with: { (snap) in
                let parqueaderoT = snap.value as? [String : AnyObject] ?? [:]
                
                let numparq = snap.key
                let estadoo = parqueaderoT["Estado"] as? String
                var horaIng = "01:00 am"
                
                    self.lots.append(parqueadero(placa:"" ,marca:"" , apartamento:"" , visitante:"", parqueadero:numparq , foto:"" , estado:estadoo, horaIngreso:horaIng))
                    
                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                    // print(self.lots.count)
                    //}
                    DispatchQueue.main.async {
                        self.grid.reloadData()
                    }
                    
                })
                
           
            
        }
        
        DispatchQueue.global(qos: .userInteractive).async {
            ref?.child("Conjuntos").child("Torres de la colina").child("ParqueaderosVisitantes").observe(.childChanged , with: { (snap) in
                let parqueaderoT = snap.value as? [String : AnyObject] ?? [:]
                // print(snap.debugDescription)
                let estadoo = parqueaderoT["Estado"] as? String
                //print(estadoo)
                
                
                let numparq = snap.key
                
                let horaIng = parqueaderoT["horaingreso"]! as? String
                let apartamentoa = parqueaderoT["apto"]! as? String
                let nickname = parqueaderoT["Visitante"]! as? String
                let nick = nickname?.lowercased()
                let fotoo = "carro"
                ref.child("Perfiles").child(nick!).observeSingleEvent(of: .value, with: { (snap) in
                    
                    //print(snap.debugDescription)
                    
                    let placaaux =  snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "placa").value as! String
                    
                    let marcaaux = snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "marca").value as! String
                    var a = 0
                    var numpq = self.lots.count
                    var salir = false
                    if(self.lots.count == 1)
                    {
                        if(self.lots[0].parqueadero == numparq)
                        {
                            self.lots.remove(at:0)
                        }
                    }
                    else if (self.lots.count == 0)
                    {
                        print("parqueaderos desocupados")
                    }
                    else if (self.lots.count > 1 )
                    {
                        while(salir == false && a < self.lots.count)
                        {
                            if(self.lots[a].parqueadero == numparq)
                            {
                                self.lots.remove(at:a)
                                salir = true
                            }
                            a = a+1
                        }
                        
                        
                    }
                    
                    
                    
                    if(placaaux != nil && marcaaux != nil && apartamentoa != nil && nick != nil && numparq != nil && fotoo != nil && estadoo != nil)
                    {
                        self.lots.append(parqueadero(placa:placaaux ,marca:marcaaux , apartamento:apartamentoa , visitante:nick, parqueadero:numparq , foto:fotoo , estado:estadoo, horaIngreso:horaIng))
                        DispatchQueue.main.async {
                            self.grid.reloadData()
                        }
                        
                    }
                    
                    //let fotoaux = snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "fotocarro").value as? String
                    //foto = fotoaux!
                    
                    
                    
                    
                })
                
                
                
                DispatchQueue.main.async {
                    self.grid.reloadData()
                }
                
                
                
                
            })
        }
        //*/
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.lots.count
    }

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TimersCollectionViewCell
        let hora = self.lots[indexPath.row].horaIngreso!
        var hour = hora.split(separator: ":")
        let components = NSDateComponents()
        components.hour = (hour[0] as NSString).integerValue
        components.minute = (hour[1] as NSString).integerValue
        components.second = 0
        var fecha = NSCalendar.current.date(from: components as DateComponents)
        
        let calendar = NSCalendar.current
        let date = Date()
        
        //            print(date.compare(fecha!))
        
        let formatter = DateComponentsFormatter()
        print("resultado formatter")
        var x = formatter.string(from: date, to: fecha!)?.split(separator: "-")
        var y =  "" + x![1] + ""
        let alert3 = UIAlertController(title: "Tiempo transcurrido ", message: y, preferredStyle: .alert)
        alert3.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> TimersCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! TimersCollectionViewCell
    
        // Configure the cell
    
        //let spot = lots[indexPath.row]
        let est = self.lots[indexPath.row].estado!
        let spot = self.lots[indexPath.row].parqueadero!
        let hora = self.lots[indexPath.row].horaIngreso!
        if(est == "Ocupado")
        {
             cell.backgroundColor = #colorLiteral(red: 1, green: 0.7307798266, blue: 0.7761104703, alpha: 1)
        }
        else{
            cell.backgroundColor = #colorLiteral(red: 0.748375833, green: 1, blue: 0.7562029958, alpha: 1)
        }
        
        var hour = hora.split(separator: ":")
        let components = NSDateComponents()
        components.hour = (hour[0] as NSString).integerValue
        components.minute = (hour[1] as NSString).integerValue
        components.second = 0
        var fecha = NSCalendar.current.date(from: components as DateComponents)
        
        let calendar = NSCalendar.current
        let date = Date()
        
        //            print(date.compare(fecha!))
        
        let formatter = DateComponentsFormatter()
        print("resultado formatter")
        var x = formatter.string(from: date, to: fecha!)?.split(separator: "-")
        var y =  ""
        // print("han transcurrido "  + x![1] + " horas")
        var rta = formatter.string(from: date, to: fecha!)
        cell.setData(parking:spot , estado: est, tiempo: y)
        
        
        return cell
    }

    
    func setupGridView() {
        let flow = collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       // let width = self.calculateWith()
        return CGSize(width: 150, height: 150)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        
        let margin = CGFloat(cellMarginSize * 2)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        
        return width
    }

  
    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
           let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }



}

    
    
   


