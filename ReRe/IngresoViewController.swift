//
//  IngresoViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/23/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class IngresoViewController: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var pass: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func registrarse(_ sender: Any) {
        
       
        let authUI = FUIAuth.defaultAuthUI()
        guard authUI != nil else {
            return
        }
        // You need to adopt a FUIAuthDelegate protocol to receive callback
        authUI?.delegate = self
        
        let authViewControoler = authUI!.authViewController()
        
        present(authViewControoler,animated: true,completion: nil)
        
        
       
    }
    
    
    @IBAction func iniciarSesion(_ sender: Any) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension IngresoViewController: FUIAuthDelegate{
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if error != nil{
            return
        }
        //authDataResult?.user.uid
        performSegue(withIdentifier: "goHome", sender: self)
    }
    
}
