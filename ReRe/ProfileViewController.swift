//
//  ProfileViewController.swift
//  ReRe
//
//  Created by Diego  Rodriguez on 10/7/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import Firebase

class ProfileViewController: UIViewController {

    
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var apellido: UITextField!
    @IBOutlet weak var cedula: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var conjunto: UITextField!
    @IBOutlet weak var apto: UITextField!
    @IBOutlet weak var placa: UITextField!
    @IBOutlet weak var marca: UITextField!
    @IBOutlet weak var modelo: UITextField!
    @IBOutlet var vc: UIView!
    
    var currentuser = usuarioActual.nickname[0]
    override func viewDidLoad() {
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        var ref: DatabaseReference!
        ref = Database.database().reference()
        super.viewDidLoad()
        alertaInternet()
        nombre.isUserInteractionEnabled = false
        apellido.isUserInteractionEnabled = false
        cedula.isUserInteractionEnabled = false
        email.isUserInteractionEnabled = false
        conjunto.isUserInteractionEnabled = false
        apto.isUserInteractionEnabled = false
        placa.isUserInteractionEnabled = false
        marca.isUserInteractionEnabled = false
        modelo.isUserInteractionEnabled = false
        
        ref.child("Perfiles").child(currentuser).observeSingleEvent(of: .value) { (snap) in
            
            let perf = snap.value as? [String : AnyObject] ?? [:]
            self.nombre.text = perf["nombre"] as! String
            self.apellido.text = perf["apellido"] as! String
            self.cedula.text = perf["cedula"] as! String
            self.email.text = perf["email"] as! String
            self.conjunto.text = perf["conjunto"] as! String
            self.apto.text = perf["apto"] as! String
            
            let car = snap.childSnapshot(forPath: "vehiculo").value as? [String : AnyObject] ?? [:]
            self.placa.text = car["placa"] as! String
            self.marca.text = car["marca"] as! String
            self.modelo.text = car["modelo"] as! String
            
            self.vc.reloadInputViews()
        }
        ref.child("Perfiles").observeSingleEvent(of: .childChanged) { (snap) in
            
            
            let perf = snap.value as? [String : AnyObject] ?? [:]
            if(self.currentuser == snap.key)
            {
                self.nombre.text = perf["nombre"] as! String
                self.apellido.text = perf["apellido"] as! String
                self.cedula.text = perf["cedula"] as! String
                self.email.text = perf["email"] as! String
                self.conjunto.text = perf["conjunto"] as! String
                self.apto.text = perf["apto"] as! String
                
                let car = snap.childSnapshot(forPath: "vehiculo").value as? [String : AnyObject] ?? [:]
                self.placa.text = car["placa"] as! String
                self.marca.text = car["marca"] as! String
                self.modelo.text = car["modelo"] as! String
            }
            
            
            
            self.vc.reloadInputViews()
        }
        
        
        
        
//        nombre.text = "Hola linc"
  //      nombre.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
    }
    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    @IBAction func cerrarSesion(_ sender: Any) {
          usuarioActual.nickname.removeAll()
          usuarioActual.apto.removeAll()
          usuarioActual.conjunto.removeAll()
        
    }
    func alertaInternet() {
        if !hayInternet() {
            let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }
}
