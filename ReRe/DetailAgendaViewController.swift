//
//  DetailAgendaViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/30/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
class DetailAgendaViewController: UIViewController {

    @IBOutlet weak var cancelarVisita: UIButton!
    @IBOutlet weak var hora: UITextField!
    @IBOutlet weak var fecha: UITextField!
    @IBOutlet weak var parqueadero: UITextField!
    @IBOutlet weak var Residente: UITextField!
    @IBOutlet weak var QRimage: UIImageView!
    var keyVisita = ""
    var objeto = visitaSolicitud()
    
    @IBAction func cancelarVisita(_ sender: Any)
    {
        print("Cancelar")
        var key = objeto.key
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        let alerta = UIAlertController(title: "Estas seguro?", message: "Estas cancelando esta visita", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
            print("You've pressed default");
            
            
            ref?.child("Visitas").child(key!).updateChildValues(["EstadoVisita" : "Cancelada"])
            
            self.performSegue(withIdentifier: "toMain", sender: self)
            
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alerta.addAction(ok)
        alerta.addAction(cancel)
        self.present(alerta, animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //salida o entrada, parqueader, nicknamevisitante, tipo parqueadero, conjunto
      // print(objeto)
        let hecho = "entrada"
        let par = "99"
        let nickV = usuarioActual.nickname[0]
        let tipoPark = "ParqueaderosVisitantes"
        let Conjunto = objeto.Conjunto!
        self.fecha.text = objeto.fecha
        self.hora.text = objeto.hora
        self.parqueadero.text = objeto.Parqueadero + " necesita parqueadero"
        self.Residente.text = objeto.nicknameResidente
       
        let qr = "\(hecho),\(par),\(nickV),\(tipoPark),\(Conjunto)"
        //print(qr)
        QRimage.image = generateQRCode(from: qr)

        // Do any additional setup after loading the view.
    }
    
    
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
