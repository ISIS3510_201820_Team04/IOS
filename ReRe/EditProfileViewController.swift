//
//  EditProfileViewController.swift
//  ReRe
//
//  Created by Diego  Rodriguez on 10/8/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class EditProfileViewController: UIViewController {
    
    @IBOutlet weak var depa: UITextField!
    @IBOutlet weak var nombre: UITextField!
    @IBOutlet weak var apellido: UITextField!
    @IBOutlet weak var cedula: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var conjunto: UITextField!
    @IBOutlet weak var apto: UITextField!
    @IBOutlet weak var placa: UITextField!
    var currentuser = usuarioActual.nickname[0]
    @IBOutlet weak var modelo: UITextField!
    @IBOutlet weak var marca: UITextField!
    @IBOutlet weak var emaiil: UITextField!
    @IBOutlet weak var carrito: UIButton!
    @IBOutlet weak var motico: UIButton!
    @IBOutlet weak var nada: UIButton!
    var vehicule = ""
    
    
    @IBAction func carritoSelected(_ sender: Any) {
        nada.alpha = 0.3
        motico.alpha = 0.3
        carrito.alpha = 1
        vehicule = "Carro"
    }
    
    @IBAction func moticoSelected(_ sender: Any) {
        nada.alpha = 0.3
         motico.alpha = 1
        carrito.alpha = 0.3
        vehicule = "Moto"
    }
    @IBAction func nadaSelected(_ sender: Any) {
        carrito.alpha = 0.3
        motico.alpha = 0.3
        nada.alpha = 1
        vehicule = "Ninguno"
    }
    
    @IBAction func SaveInfo(_ sender: Any) {
        var bd: DatabaseReference!
        bd = Database.database().reference()
        let correo = emaiil.text
        let marcas = marca.text
        let modelos = modelo.text
        let placas = placa.text
//        var valido = false
        
        if ((marcas!.count) < 3 || (marcas!.count) > 50){
            displayAlertMessage(message: "La marca debe tener entre 3 y 30 caracteres");
            return;
        }
        if ((modelos!.count) < 3 || (modelos!.count) > 50){
            displayAlertMessage(message: "El modelo debe tener entre 3 y 30 caracteres");
            return;
        }
        if ((placas!.count) < 3 || (placas!.count) > 10){
            displayAlertMessage(message: "La placa debe tener entre 3 y 30 caracteres");
            return;
        }
        
        if(isValidEmail(testStr: correo!))
        {
            if ((correo?.count)! < 6 || (correo?.count)! > 30 )
            {
                let alert3 = UIAlertController(title: "Error", message: "email maximun lenght is 30 characters, and minimun lenght is 6 characters", preferredStyle: .alert)
                alert3.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                    
                    
                }))
                
                self.present(alert3, animated: true, completion: nil)
            }
            else
            {
                
                //print(self.cedula.text)
                bd.child("Perfiles").child(currentuser).updateChildValues( ["email":correo!])
                bd.child("Perfiles").child(currentuser).child("vehiculo").updateChildValues( ["marca":marcas!,"placa":placas!,"modelo":modelos!])
                
                
                
                
                let alert3 = UIAlertController(title: "Notificación", message: "Perfil actualizado", preferredStyle: .alert)
                alert3.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                    
                    
                }))
                
                self.present(alert3, animated: true, completion: nil)
            }
        }
       
        else
        {
            let alert4 = UIAlertController(title: "Notificación", message: "Correo invalido", preferredStyle: .alert)
            alert4.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
                
                
            }))
            
            self.present(alert4, animated: true, completion: nil)
        }
        
        
    }
    public func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func botonRegistroPress(_ sender: Any) {
        
        
    }
    
    func displayAlertMessage(message:String){
        let alert = UIAlertController(title: "Alerta", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        placa.isUserInteractionEnabled = true
        marca.isUserInteractionEnabled = true
        modelo.isUserInteractionEnabled = true
        conjunto.isUserInteractionEnabled = false
        depa.isUserInteractionEnabled = false
        nombre.isUserInteractionEnabled = false
        apellido.isUserInteractionEnabled = false
        cedula.isUserInteractionEnabled = false
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        ref.child("Perfiles").child(currentuser).observeSingleEvent(of: .value) { (snap) in
            
            let perf = snap.value as? [String : AnyObject] ?? [:]
            self.nombre.text = perf["nombre"] as! String
            self.apellido.text = perf["apellido"] as! String
            self.cedula.text = perf["cedula"] as! String
            self.email.text = perf["email"] as! String
            self.conjunto.text = perf["conjunto"] as! String
            self.depa.text = perf["apto"] as! String
            
            let car = snap.childSnapshot(forPath: "vehiculo").value as? [String : AnyObject] ?? [:]
            self.placa.text = car["placa"] as! String
            self.marca.text = car["marca"] as! String
            self.modelo.text = car["modelo"] as! String
            
            
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
