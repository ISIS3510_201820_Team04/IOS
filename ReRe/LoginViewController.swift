//
//  LoginViewViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 25/09/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//


import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn
import Foundation
import SystemConfiguration

class LoginViewController: UIViewController,  GIDSignInUIDelegate{

    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var pass: UITextField!
   // @IBOutlet weak var loginnormail: UIButton!
    @IBOutlet weak var logincelcho: GIDSignInButton!
    
    @IBOutlet weak var google: GIDSignInButton!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertaInternet()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
       // googleButton()
       
    }
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
            let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }
    
   
    
    fileprivate func googleButton(){
        let button = GIDSignInButton()
        button.frame = CGRect(x:60, y:50, width:view.frame.width - 115, height:50)
        view.addSubview(button)
        
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func iniciarSesion(_ sender: Any) {
        
        if( (username.text!.isEmpty) || (pass.text!.isEmpty) ){
            displayAlertMessage(message: "Todos los campos son requeridos");
            return;
        }
        if( (username.text!.contains(".")) || (username.text!.contains("#")) || (username.text!.contains("$")) || (username.text!.contains("[")) || (username.text!.contains("]")) ){
            displayAlertMessage(message: "El nombre de usuario no puede contener los siguientes caracteres '.' '#' '$' '[' or ']'");
            return;
        }
        
        
        ref = Database.database().reference()
        usuarioActual.ref = [ref]
        ref.child("Perfiles").child(username.text!).observeSingleEvent(of: .value, with: { (snap) in
            
            if(snap.exists())
            {
                let perfil = snap.value as? [String : AnyObject] ?? [:]
                let conjunto = perfil["conjunto"] as? String
                let apto = perfil["apto"] as? String
                let passw = perfil["pass"] as? String
                if(self.pass.text == passw)
                {
                    usuarioActual.nickname.append(self.username.text!)
                    usuarioActual.conjunto.append(conjunto!)
                    usuarioActual.apto.append(apto!)
//                    self.cuadrarVisitas()

                    
                    self.performSegue(withIdentifier: "logintohome", sender:self )
                }
                else{
                    
                    let alert = UIAlertController(title: "Error", message: "Contraseña Incorrecta", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else{
                let alert = UIAlertController(title: "Error", message: "Usuario ingresado no existe", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            
        })
           
        
    }
    
    func cuadrarVisitas(){
        
        self.ref = Database.database().reference()
        let currentuser  = usuarioActual.nickname[0]
        var si = true
        //let conjunto = usuarioActual.conjunto[0]
       DispatchQueue.global(qos: .userInteractive).async {
        
            
            
            
        self.ref.child("Visitas").observe(.childAdded , with: { (snap) in
                let visita = snap.value as? [String : AnyObject] ?? [:]
                //let numparq = snap.key
                let keyt = snap.key
                let est = visita["EstadoVisita"] as? String
                let fec = visita["FechaVisita"] as? String
                let hor = visita["HoraVisita"] as? String
                let nickR = visita["IdRecidente"] as? String
                var nickV = visita["IdVisitante"] as? String
                let park = visita["Parqueadero"] as? String
                let conjunto = visita["Conjunto"] as? String
                
                
                if((nickV == currentuser || nickR == currentuser) && est == "Aceptada")                {
                    let nuevo = visitaSolicitud(key:keyt,estado:est ,fecha:fec , hora:hor , nicknameResidente:nickR, nicknameVisitante:nickV , Parqueadero:park, Conjunto:conjunto )
                    
                    usuarioActual.visitas.append(nuevo)
                    
                    si = false
                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                   
                    //}
                }
                
              
               
                // print(usuarioActual.visitas.count)
                
            })
        sleep(1)
//        if (si){
//            
//        }else{
////
////          let nuevo = visitaSolicitud(key:"",estado:"" ,fecha:"" , hora:"" , nicknameResidente:"No tienes visitas", nicknameVisitante:"No tienes visitas" , Parqueadero:"", Conjunto:"" )
////
////            usuarioActual.visitas.append(nuevo)
//        }
      }
    }
    
    @IBAction func ok(_ sender: Any) {
        
    }
    
   
    /*@IBAction func login(_ sender: Any) {
        let user = username.text
        let password = pass.text
        print(user!,password!)
        Auth.auth().signIn(withEmail: user!, password: password!) { (user, error) in
            if user != nil{
                print("melo caramelo")
                self.displayAlertMessage(message: "todo ok");
            }
            if error != nil{
                self.displayAlertMessage(message: error.debugDescription );
            }
           print(user)
            print("--------------------------------")
           
        }
        print("putos todos")
        
    }*/
    

    func displayAlertMessage(message:String){
        let alert = UIAlertController(title: "Atención", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
