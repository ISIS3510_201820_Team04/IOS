//
//  ParkingViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/25/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import SystemConfiguration

class ParkingViewController: UIViewController {

    @IBOutlet weak var interruptor: UISwitch!
    
    var ref: DatabaseReference!
    let currentUser = "1010229655"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertaInternet()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        self.ref = Database.database().reference()
        self.ref.child("Perfiles").child(self.currentUser).observeSingleEvent(of: .value, with: { (snap) in
            let per = snap.value as? [String : AnyObject] ?? [:]
            let apto = per["apto"] as! String
            let conjunto = per["conjunto"] as! String
            
            self.ref?.child("Conjuntos").child(conjunto).child("ParqueaderosPropietarios").observeSingleEvent(of: .value, with:{
                (snap) in
               // print(snap.debugDescription)
                
                for rest in snap.children.allObjects as! [DataSnapshot]
                {
                    let parq = rest.value as? [String : AnyObject] ?? [:]
                    let appa = parq["apto"] as! String
                    let estado = parq["Estado"] as! String
                    let prestamo = parq["prestamo"] as! String
                    let numpar = rest.key
                    
                    if(appa == apto && estado == "Disponible" && prestamo  == "true")
                    {
                        self.interruptor.setOn(true, animated: true)
                    }
                }
                
                
                
            })
        })
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func changeparkingStatus(_ sender: Any)
    {
        self.ref = Database.database().reference()
        
        if(interruptor.isOn)
        {
            
            let refreshAlert = UIAlertController(title: "Share parking spot", message: "Are you sure that you want to share your parking spot?", preferredStyle: UIAlertController.Style.alert)
            
            
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                self.interruptor.setOn(false, animated: true)
            }))
            refreshAlert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { (action: UIAlertAction!) in
                //print("Write on database")
                
                self.ref.child("Perfiles").child(self.currentUser).observeSingleEvent(of: .value, with: { (snap) in
                    let per = snap.value as? [String : AnyObject] ?? [:]
                    let apto = per["apto"] as! String
                    let conjunto = per["conjunto"] as! String
                    
                    self.ref?.child("Conjuntos").child(conjunto).child("ParqueaderosPropietarios").observeSingleEvent(of: .value, with:{
                        (snap) in
                        //print(snap.debugDescription)
                        
                        for rest in snap.children.allObjects as! [DataSnapshot]
                        {
                            let parq = rest.value as? [String : AnyObject] ?? [:]
                            let appa = parq["apto"] as! String
                            let estado = parq["Estado"] as! String
                            let numpar = rest.key
                            
                            if(appa == apto && estado == "Disponible")
                            {
                                self.ref.child("Conjuntos").child(conjunto).child("ParqueaderosPropietarios").child(numpar).updateChildValues(["prestamo" : "true"])
                            }
                        }
                        
                        
                        
                    })
                })
                
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            //print("se apago")
            
            var refreshAlert = UIAlertController(title: "Share parking spot", message: "Are you sure that you don't want to share your parking spot?", preferredStyle: UIAlertController.Style.alert)
            
            
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                self.interruptor.setOn(true, animated: true)
            }))
            refreshAlert.addAction(UIAlertAction(title: "Accept", style: .default, handler: { (action: UIAlertAction!) in
                //print("Write on database")
                
                self.ref.child("Perfiles").child(self.currentUser).observeSingleEvent(of: .value, with: { (snap) in
                    let per = snap.value as? [String : AnyObject] ?? [:]
                    let apto = per["apto"] as! String
                    let conjunto = per["conjunto"] as! String
                    
                    self.ref?.child("Conjuntos").child(conjunto).child("ParqueaderosPropietarios").observeSingleEvent(of: .value, with:{
                        (snap) in
                        //print(snap.debugDescription)
                        
                        for rest in snap.children.allObjects as! [DataSnapshot]
                        {
                            let parq = rest.value as? [String : AnyObject] ?? [:]
                            let appa = parq["apto"] as! String
                            let estado = parq["Estado"] as! String
                            let numpar = rest.key
                            
                            if(appa == apto && estado == "Disponible")
                            {
                                self.ref.child("Conjuntos").child(conjunto).child("ParqueaderosPropietarios").child(numpar).updateChildValues(["prestamo" : "false"])
                            }
                        }
                        
                        
                        
                    })
                    
                    let alert3 = UIAlertController(title: "Notification", message: "Your parking spot will be avalaible in about 20 minutes", preferredStyle: .alert)
                    alert3.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert3, animated: true, completion: nil)
                })
                
            }))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        
        
    }
    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
            let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }
    
}
