//
//  VisitViewController.swift
//  ReRe
//
//  Created by Diego  Rodriguez on 10/7/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import SystemConfiguration

class VisitViewController: UIViewController {

    var x = friend.init()
    var currentUser = ""
    var conjunto = ""
    
    
    @IBOutlet weak var nombre: UILabel!
    @IBOutlet weak var fecha: UIDatePicker!
    @IBOutlet weak var interruptor: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        alertaInternet()
        nombre.text = x.name
    }
    
    @IBAction func save(_ sender: Any)
    {
        var ref: DatabaseReference!
        ref = Database.database().reference()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        let visitante = currentUser
        let propietario = x.nickname
        let conjunto = x.item
        let horallegadaestimada = fecha.date
        var horallegada = ""
        var parqueadero = "NO"
        if(interruptor.isOn)
        {
            parqueadero = "SI"
        }
        let fechi = Date()
        let format = DateFormatter()
        format.dateStyle = .short
        format.timeStyle = .none
        let format2 = DateFormatter()
        format2.dateStyle = .none
        format2.timeStyle = .short
        let fechavisita = format.string(from: fechi)
        horallegada = format2.string(from: horallegadaestimada)
        
        
        
        
        let key = ""+visitante+propietario!+horallegada
        
        
        ref.child("Visitas").child(key).setValue(["EstadoVisita": "Enviada", "FechaVisita": fechavisita, "HoraVisita": horallegada, "IdRecidente":propietario, "IdVisitante":visitante, "Parqueadero":parqueadero, "Conjunto": conjunto])
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "contacts")
        navigationController?.isNavigationBarHidden = false
        navigationController?.pushViewController(destination, animated: true)
       
        
    
        let alert3 = UIAlertController(title: "Notificación", message: "Su visita ha sido creada queda a la espera de aprobación", preferredStyle: .alert)
        alert3.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
            
        }))
        self.present(alert3, animated: true, completion: nil)
        
    }
    
    
    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
           let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }

}
