//
//  ContactsTableViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 9/20/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import SystemConfiguration

class ContactsTableViewController: UITableViewController,UISearchBarDelegate {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet var fs: UITableView!
    var con : [friend] = []
    var contactosActual = [friend]()
    var busqueda = false
    var currentuser = usuarioActual.nickname[0]
    func arreglos()
    {}
        
    override func viewDidLoad() {
        super.viewDidLoad()
        alertaInternet()
        
        // DATABASE CONECTION
        var ref: DatabaseReference!
        ref = Database.database().reference()
       // var tempo : [friend] = []
        //cuando se haga el login se hace la busqueda de los contactos del usuario actual
        
        DispatchQueue.global(qos: .userInteractive).async {
            ref?.child("Perfiles").child(self.currentuser).child("contactos").observe(.childAdded, with: { (snapshot) in
                
                //print(snapshot.debugDescription)
                
                let postDict = snapshot.value as? [String : AnyObject] ?? [:]
                let cedulaC = postDict["nickname"] as? String
                //  print("nickname de contacto")
                // print(cedulaC)
                let nickname = cedulaC?.lowercased()
                
                ref.child("Perfiles").child(nickname!).observeSingleEvent( of: .value, with: { (info) in
                    
                    //  print(info.debugDescription)
                    // print(info.childSnapshot(forPath: "direccion").debugDescription)
                    
                    var direction = info.childSnapshot(forPath: "conjunto").value as? String
                    
                    let nombreT = info.childSnapshot(forPath: "nombre").value as? String
                    
                    if(direction == nil)
                    {
                        direction = ""
                    }
                    
                    // print(nombreT)
                    //print(direction)
                    // print("acabo de imprimir")
                    if(nombreT != nil && direction != nil)
                    {
                        let contact = friend(name: nombreT!, item: direction!, image: "conjunto 2", nickname:nickname!)
                        // print(contact.name)
                        //print(contact.image)
                        // print(contact.item)
                        self.con.append(contact)
                        //print("numero de contactos")
                        // print(self.con.count)
                        self.tableView.reloadData()
                    }
                })
            })
        }
        
    }
    
    @IBAction func addContact(_ sender: Any)
    {
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        let alert = UIAlertController(title: "Agregar Contacto", message: "Ingresa el nickname de tu amigo", preferredStyle: .alert)
        alert.addTextField { (textfield) in
            textfield.placeholder = "nickname"
        }
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Agregar", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
            let x = alert.textFields![0].text
            //BUSCAR EN LA BASE DE DATOS
            if(x == "")
            {
                return
                
            }
           
            ref.child("Perfiles").child(x!).observeSingleEvent( of: .value, with: { (info) in
                if(info.exists())
                {
                    
                    ref.child("Perfiles").child(self.currentuser).child("contactos").child(x!).setValue(["nickname": x!])
                    let alert2 = UIAlertController(title: "Notification", message: "Contact successfully added", preferredStyle: .alert)
                    alert2.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert2, animated: true, completion: nil)
                }
                else
                {
                    let alert3 = UIAlertController(title: "Notification", message: "There are not users with this nickname", preferredStyle: .alert)
                    alert3.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alert3, animated: true, completion: nil)
                }
            })
            
            
            
            
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if busqueda {
            return contactosActual.count
        } else {
            return con.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destination = storyboard.instantiateViewController(withIdentifier: "visita") as! VisitViewController
        
        let contactoObject = con[indexPath.row]
        destination.currentUser = currentuser
        destination.x = contactoObject
        navigationController?.pushViewController(destination, animated: true)
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ContactTableViewCell
        
        if busqueda {
            
            let contactoObject = contactosActual[indexPath.row]
            cell.name.text = contactoObject.name
            cell.item.text = contactoObject.item
            cell.backgroundImage.image = UIImage(named: contactoObject.image!)
        } else {
            let contactoObject = con[indexPath.row]
            cell.name.text = contactoObject.name
            cell.item.text = contactoObject.item
            cell.backgroundImage.image = UIImage(named: contactoObject.image!)
        }
        return cell
        
        
        
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        busqueda = true
        contactosActual = con.filter { $0.name.lowercased().prefix(searchText.count) == searchText.lowercased()}
        fs.reloadData()
        
        
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    
    }
    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
            let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }
    
    
}

