//
//  AgendaTableViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/30/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class AgendaTableViewController: UITableViewController {
    
    var currentuser  = usuarioActual.nickname[0]
    var conjunto = usuarioActual.conjunto[0]
    var visitas : [visitaPersona] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        DispatchQueue.global(qos: .userInteractive).async {
            
            
           
            
            ref?.child("Visitas").observe(.childAdded , with: { (snap) in
                let visita = snap.value as? [String : AnyObject] ?? [:]
                //print(visita)
                //let numparq = snap.key
                let est = visita["EstadoVisita"] as? String
                let fec = visita["FechaVisita"] as? String
                let hor = visita["HoraVisita"] as? String
                let nickR = visita["IdRecidente"] as? String
                var nickV = visita["IdVisitante"] as? String
                let park = visita["Parqueadero"] as? String
                let conju = visita["Conjunto"] as? String
                
                
                if(nickV == self.currentuser && est == "Aceptada")
                {
                    let nuevo = visitaPersona(estado:est ,fecha:fec , hora:hor , nicknameResidente:nickR, nicknameVisitante:nickV , Parqueadero:park, Conjunto:conju)
                    
                    self.visitas.append(nuevo)
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                    // print(self.visitas.count)
                    //}
                }
                
                
                
                
            })
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //print(self.visitas.count)
        return self.visitas.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Agenda", for: indexPath) as! AgendaTableViewCell
        let actual =  self.visitas[indexPath.row]

//        cell.linea1.text = actual.estado!
        cell.linea2.text = actual.nicknameResidente!
        cell.linea3.text = "a las: " + actual.hora
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //salida o entrada, parqueader, nicknamevisitante, tipo parqueadero, conjunto
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let destion = storyboard.instantiateViewController(withIdentifier: "detailQR") as! DetailAgendaViewController
       
        let contactoObject = visitas[indexPath.row]
        //destination.currentUser = currentuser
      //  destion.objeto = contactoObject
        //performSegue(withIdentifier: "AgendaDetail", sender: self)
        navigationController?.pushViewController(destion, animated: true)
    }
   
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
