//
//  HomePageViewController.swift
//  ReRe
//
//  Created by Diego  Rodriguez on 10/6/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import SystemConfiguration
import Speech



class HomePageViewController: UIViewController {

    
    @IBOutlet weak var micButton: UIButton!
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en-ES")) //1
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private var audioEngine = AVAudioEngine()
   // var lang: String = "en-US"
    var lang: String = "es-ES"
    var comando = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertaInternet()
        
//        print(usuarioActual.apto)
//         print(usuarioActual.nickname)
//         print(usuarioActual.conjunto)
//        print(usuarioActual.conjunto[0])
        //micButton.isEnabled = false  //2
        speechRecognizer?.delegate = self as? SFSpeechRecognizerDelegate
        // print("inicccc")
    }
    
   
    
    /* diferentes idiomas
    
    @IBAction func segmentAct(_ sender: Any) {
        switch segmentCtr.selectedSegmentIndex {
        case 0:
            lang = "en-US"
            break;
        case 1:
            lang = "es-ES"
            break;
        default:
            lang = "en-US"
            break;
        }
        
       
    }
    */
    
    @IBAction func empieza(_ sender: Any) {
   
        speechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang))
        
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            micButton.isEnabled = false
            micButton.isSelected = false
            //micButton.setTitle("Start Recording", for: .normal)
        } else {
            startRecording()
            micButton.isSelected = true
            //startStopBtn.setTitle("Stop Recording", for: .normal)
        }
    }
    
    
    func startRecording() {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.record, mode: .default)
            try audioSession.setMode(AVAudioSession.Mode.measurement)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer?.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            
            var isFinal = false
            
            if result != nil {
                let resultado = (result?.bestTranscription.formattedString)!
                //print(resultado)
                if (resultado.contains("cronograma")||resultado.contains("Cronograma")){
                    let alerta = UIAlertController(title: "Alerta", message: "Ir a cronograma?", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.performSegue(withIdentifier: "cronograma", sender: self)
                    })
                    let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
                    alerta.addAction(ok)
                    alerta.addAction(cancel)
                    self.present(alerta, animated: true, completion: nil)
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                    self.micButton.isEnabled = false
                    self.micButton.isSelected = false
                   
                }else if (resultado.contains("solicitudes")||resultado.contains("Solicitudes")){
                    let alerta = UIAlertController(title: "Alerta", message: "Ir a solicitudes?", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                       self.performSegue(withIdentifier: "voiceRequest", sender: self)
                          })
                    let cancel = UIAlertAction(title: "Cancelar", style: .default, handler: nil)
                    alerta.addAction(ok)
                    alerta.addAction(cancel)
                    self.present(alerta, animated: true, completion: nil)
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                    self.micButton.isEnabled = false
                    self.micButton.isSelected = false
                }else if( resultado.contains("parqueaderos")||resultado.contains("Parqueaderos")){
                    let alerta = UIAlertController(title: "Alerta", message: "Ir a parqueaderos?", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.performSegue(withIdentifier: "voicePark", sender: self)
                        })
                    let cancel = UIAlertAction(title: "Cancelar", style: .default, handler:nil)
                    alerta.addAction(ok)
                    alerta.addAction(cancel)
                    self.present(alerta, animated: true, completion: nil)
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                    self.micButton.isEnabled = false
                    self.micButton.isSelected = false
                }else if (resultado.contains("contactos")||resultado.contains("Contactos")){
                    print("4")
                    let alerta = UIAlertController(title: "Lo sentimos", message: "Estamos trabajando es esta funcionalidad", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                       // self.performSegue(withIdentifier: "voiceContacts", sender: self)
                    })
                   // let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alerta.addAction(ok)
                    //alerta.addAction(cancel)
                    self.present(alerta, animated: true, completion: nil)
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                    self.micButton.isEnabled = false
                    self.micButton.isSelected = false
                }else if (resultado.contains("Perfil")||resultado.contains("perfil")){
                    let alerta = UIAlertController(title: "Sorry", message: "Estamos trabajando es esta funcionalidad", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                        //self.performSegue(withIdentifier: "voiceProfile", sender: self)
                    })
                   // let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alerta.addAction(ok)
                   // alerta.addAction(cancel)
                    self.present(alerta, animated: true, completion: nil)
                    self.audioEngine.stop()
                    self.recognitionRequest?.endAudio()
                    self.micButton.isEnabled = false
                    self.micButton.isSelected = false
                }
                
                
                
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.micButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        do {
            try audioEngine.start()
        } catch {
            print("audioEngine couldn't start because of an error.")
        }
        
      //  print( "Say something, I'm listening!")
        
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            micButton.isEnabled = true
        } else {
            micButton.isEnabled = false
        }
    }
    
    
    
    
    
    
    
    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
           let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
