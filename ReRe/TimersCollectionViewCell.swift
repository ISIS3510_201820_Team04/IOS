//
//  TimersCollectionViewCell.swift
//  ReRe
//
//  Created by Julian Camilo on 5/10/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit


class TimersCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var parkingSpot: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func draw(_ rect: CGRect) { //Your code should go here.
        super.draw(rect)
        self.layer.cornerRadius = self.frame.size.width / 2
    }
    
    func setData(parking: String,estado: String , tiempo: String) {
        self.parkingSpot.text = parking
        self.status.text = estado
        self.time.text = tiempo
    }
    
    func setBackground(color: UIColor) {
        backgroundColor = color
    }
    
   
    
    
}
