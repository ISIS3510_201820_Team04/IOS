//
//  AgendaTableViewCell.swift
//  ReRe
//
//  Created by Julian Camilo on 10/30/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

class AgendaTableViewCell: UITableViewCell {

    
    @IBOutlet weak var linea1: UILabel!
    @IBOutlet weak var linea2: UILabel!
    @IBOutlet weak var linea3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
