//
//  GoogleSignInViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/24/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn


class GoogleSignInViewController: UIViewController,  GIDSignInUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        googleButton()

        // Do any additional setup after loading the view.
    }
    
    fileprivate func googleButton(){
        let button = GIDSignInButton()
        button.frame = CGRect(x:view.frame.maxX/6, y:view.frame.maxY/2, width:view.frame.width - 115, height:50)
        view.addSubview(button)
        
        GIDSignIn.sharedInstance()?.uiDelegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
