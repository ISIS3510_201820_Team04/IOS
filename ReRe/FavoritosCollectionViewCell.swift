//
//  FavoritosCollectionViewCell.swift
//  ReRe
//
//  Created by Julian Camilo on 10/25/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

class FavoritosCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imagen: UIImageView!
    
    @IBOutlet weak var Nombre: UILabel!
    
    override func draw(_ rect: CGRect) { //Your code should go here.
        super.draw(rect)
        self.layer.cornerRadius = self.frame.size.width / 2
    }
}
