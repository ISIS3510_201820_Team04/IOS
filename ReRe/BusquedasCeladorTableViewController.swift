//
//  BusquedasCeladorTableViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 25/09/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class BusquedasCeladorTableViewController: UITableViewController, UISearchBarDelegate {
    
    enum selectedScope:Int {
        case vehicle = 0
        case ps = 1
        case apartment = 2
    }

    @IBOutlet var tv: UITableView!
    var busqueda = false
    
   
    
    var parqueaderos : [parqueadero] = []
    var parqueaderosActual : [parqueadero] = []
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        self.busqueda = false
        ref = Database.database().reference()
        if(self.parqueaderos.count == 0)
        {
            DispatchQueue.global(qos:.userInteractive).async {
                self.ref?.child("Conjuntos").child("Torres de la colina").child("ParqueaderosVisitantes").observe(.childAdded , with: { (snap) in
                    let parqueaderoT = snap.value as? [String : AnyObject] ?? [:]
                    //  print(snap.debugDescription)
                    let estadoo = parqueaderoT["Estado"] as? String
                    // print(estadoo)
                    
                    if (estadoo == "Ocupado")
                    {
                        let numparq = snap.key
                        let horaIng = parqueaderoT["horaingreso"]! as? String
                        let apartamentoa = parqueaderoT["apto"]! as? String
                        
                        let nickname = parqueaderoT["Visitante"]! as? String
                        let nick = nickname?.lowercased()
                        let fotoo = "carro"
                        self.ref.child("Perfiles").child(nick!).observeSingleEvent(of: .value, with: { (snap) in
                            
                            // print(snap.debugDescription)
                            let placaaux =  snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "placa").value as? String
                            
                            let marcaaux = snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "marca").value! as? String
                            
                            
                            if(placaaux != nil && marcaaux != nil && apartamentoa != nil && nick != nil && numparq != nil && fotoo != nil && estadoo != nil)
                            {self.parqueaderos.append(parqueadero(placa:placaaux! ,marca:marcaaux! , apartamento:apartamentoa , visitante:nick, parqueadero:numparq , foto:fotoo , estado:estadoo, horaIngreso: horaIng))
                                // print(numparq)
                                DispatchQueue.main.async {
                                    self.tv.reloadData()
                                }
                                }
                            
                            
                            
                        })
                        
                        
                        
                        
                        DispatchQueue.main.async {
                            self.tv.reloadData()
                        }
                    }
                    
                    
                })
            }
            }
        
            DispatchQueue.global(qos: .userInteractive).async {
                self.ref?.child("Conjuntos").child("Torres de la colina").child("ParqueaderosVisitantes").observe(.childChanged , with: { (snap) in
                    let parqueaderoT = snap.value as? [String : AnyObject] ?? [:]
                    // print(snap.debugDescription)
                    let estadoo = parqueaderoT["Estado"] as? String
                    //print(estadoo)
                    
                    self.busqueda = false
                    let numparq = snap.key
                    
                    let horaIng = parqueaderoT["horaingreso"]! as? String
                    let apartamentoa = parqueaderoT["apto"]! as? String
                    let nickname = parqueaderoT["Visitante"]! as? String
                    let nick = nickname?.lowercased()
                    let fotoo = "carro"
                    self.ref.child("Perfiles").child(nick!).observeSingleEvent(of: .value, with: { (snap) in
                        
                        //print(snap.debugDescription)
                        
                        let placaaux =  snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "placa").value as? String
                        
                        let marcaaux = snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "marca").value! as? String
                        var a = 0
                        var numpq = self.parqueaderos.count
                        var salir = false
                        if(self.parqueaderos.count == 1)
                        {
                            if(self.parqueaderos[0].parqueadero == numparq)
                            {
                                self.parqueaderos.remove(at:0)
                            }
                        }
                        else if (self.parqueaderos.count == 0)
                        {
                            print("parqueaderos desocupados")
                        }
                        else if (self.parqueaderos.count > 1 )
                        {
                            while(salir == false && a < self.parqueaderos.count)
                            {
                                if(self.parqueaderos[a].parqueadero == numparq)
                                {
                                    self.parqueaderos.remove(at:a)
                                    salir = true
                                }
                                a = a+1
                            }
                            
                            
                        }
                        
                        
                        if(estadoo == "Ocupado")
                        {
                            if(placaaux != nil && marcaaux != nil && apartamentoa != nil && nick != nil && numparq != nil && fotoo != nil && estadoo != nil)
                            {
                                self.parqueaderos.append(parqueadero(placa:placaaux! ,marca:marcaaux! , apartamento:apartamentoa , visitante:nick, parqueadero:numparq , foto:fotoo , estado:estadoo, horaIngreso: horaIng))
                                DispatchQueue.main.async {
                                    self.tv.reloadData()
                                }
                                
                            }
                        }
                            //let fotoaux = snap.childSnapshot(forPath: "vehiculo").childSnapshot(forPath: "fotocarro").value as? String
                            //foto = fotoaux!
                            
                        else if(estadoo != "Ocupado")
                        {
                            DispatchQueue.main.async {
                                self.tv.reloadData()
                            }
                        }
                        
                        
                    })
                    
                    
                    
                    
                    DispatchQueue.main.async {
                        self.tv.reloadData()
                    }
                    
                    
                    
                })
            
        }
        
        
        
        
        

       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            parqueaderosActual = parqueaderos
            tv.reloadData()
        }else {
            busqueda = true
            filterTableView(ind: searchBar.selectedScopeButtonIndex, text: searchText)
            tv.reloadData()
        }
    }
    
    func filterTableView(ind:Int,text:String) {
        switch ind {
        case selectedScope.vehicle.rawValue:
            //fix of not searching when backspacing
            parqueaderosActual = parqueaderos.filter({ (mod) -> Bool in
                return mod.placa.lowercased().contains(text.lowercased())
            })
            tv.reloadData()
        case selectedScope.ps.rawValue:
            //fix of not searching when backspacing
            parqueaderosActual = parqueaderos.filter({ (mod) -> Bool in
                return mod.parqueadero.lowercased().contains(text.lowercased())
                
            })
            tv.reloadData()
        case selectedScope.apartment.rawValue:
            //fix of not searching when backspacing
            parqueaderosActual = parqueaderos.filter({ (mod) -> Bool in
                return mod.apartamento.lowercased().contains(text.lowercased())
            })
           tv.reloadData()
        default:
            print("no type")
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if busqueda{
            return parqueaderosActual.count
        }else{
            return parqueaderos.count
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "data", for: indexPath) as! BusquedasCeladorTableViewCell
    
        if busqueda{
            let vehi = parqueaderosActual[indexPath.row]
            cell.displayplaca.text = vehi.placa
            cell.displayApto.text = vehi.apartamento
            //cell.displayduenio.text = vehi.visitante
            cell.displayPark.text = vehi.parqueadero
            cell.carroImage.image = UIImage(named: vehi.foto!)
        
        }else{
            let vehi = parqueaderos[indexPath.row]
            cell.displayplaca.text = vehi.placa
            cell.displayApto.text = vehi.apartamento
            //cell.displayduenio.text = vehi.visitante
            //cell.displayduenio.text = vehi.duenio
            cell.displayPark.text = vehi.parqueadero
            cell.carroImage.image = UIImage(named: vehi.foto!)
        }
        
        
       // cell.imgView.image = UIImage(named: model.imageName)
        
        //cell.byLbl.text = model.imageBy
        //cell.yearLbl.text = model.imageYear
        
        
        return cell
    }
    
    /*add delegate method for pushing to new detail controller
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        vc.dataModel = dataAry[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }*/
    
    
    

}
