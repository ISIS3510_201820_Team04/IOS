//
//  HomeCeladorViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 7/10/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

class HomeCeladorViewController: UIViewController {

    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var image: UIImageView!
    var x = BusquedasCeladorTableViewController.self
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
       
        // Do any additional setup after loading the view.
    }

    @IBAction func generarQR(_ sender: Any) {
       image.image = generateQRCode(from: "PUTOOOO EL QUE LO LEAAAAAAAA")
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
