//
//  VisitasTableViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 10/25/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit
import Firebase

class VisitasTableViewController: UITableViewController {

    var visitas : [visitaSolicitud] = []
    var currentuser = usuarioActual.nickname[0]
    var conjuntoUser = usuarioActual.conjunto[0]
    var ref: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        
        DispatchQueue.global().async {
            self.ref?.child("Visitas").observe(.childAdded , with: { (snap) in
                let visita = snap.value as? [String : AnyObject] ?? [:]
                //print(visita)
                //let numparq = snap.key
                let keyt = snap.key
                let est = visita["EstadoVisita"] as? String
                let fec = visita["FechaVisita"] as? String
                let hor = visita["HoraVisita"] as? String
                let nickR = visita["IdRecidente"] as? String
                let nickV = visita["IdVisitante"] as? String
                let park = visita["Parqueadero"] as? String
                let conjunto = visita["Conjunto"] as? String
                
                
                if(nickR == self.currentuser && est == "Aceptada")
                {
                    let nuevo = visitaSolicitud(key:keyt,estado:est ,fecha:fec , hora:hor , nicknameResidente:nickR, nicknameVisitante:nickV , Parqueadero:park, Conjunto:conjunto )
                    
                    self.visitas.append(nuevo)
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                    //print("hjkdhakshdkjshdjkhsjkdhsjkdhkjhsdjhsjkhdkjsjdkdjks")
                    // print(self.visitas.count)
                    //}
                }
                
                
                
                
            })
        }
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.visitas.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Visita", for: indexPath) as! VisitasTableViewCell
        
        
        let visita =  self.visitas[indexPath.row]
        
        cell.code = visita.key
        cell.nombre.text = visita.nicknameVisitante + " esta en camino"
        cell.hora.text = "a las " + visita.hora
        if(visita.Parqueadero == "SI")
        {
            cell.necesitaPark.text = "Necesita un parqueadero"
        }
        else{
            cell.necesitaPark.text = "No necesita parqueadero"
        }// Configure the cell...
        
        return cell
       
        
       
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let cancelar = UITableViewRowAction(style: .normal, title: "Cancel") { action, index in
            
            let cell = tableView.cellForRow(at: index) as? VisitasTableViewCell
            let alerta = UIAlertController(title: "Estas seguro?", message: "Estas cancelando esta visita", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in
                print("You've pressed default");
                var key  = (cell!.code)
                
                
                self.ref?.child("Visitas").child(key).updateChildValues(["EstadoVisita" : "Cancelada"])
                
                self.visitas.remove(at: index.row)
                self.tableView.reloadData()
                
                
                
            }
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alerta.addAction(ok)
            alerta.addAction(cancel)
            self.present(alerta, animated: true, completion: nil)
        }
        cancelar.backgroundColor = .red
        
     
   
        
        return [cancelar]
    }


    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
