//
//  RegisterPageControllerViewController.swift
//  ReRe
//
//  Created by Julian Camilo on 9/13/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//  Copyright © 2018 Diego Andres. All rights reserved.
//

import UIKit
import Firebase
import Foundation
import SystemConfiguration
import FirebaseAuth


class RegisterPageControllerViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

  
    @IBOutlet weak var a: UITextField!
    @IBOutlet weak var B: UITextField!
    @IBOutlet weak var C: UITextField!
    @IBOutlet weak var D: UITextField!
    @IBOutlet weak var doc: UITextField!
    @IBOutlet weak var nombres: UITextField!
    @IBOutlet weak var apellidos: UITextField!
    
    @IBOutlet weak var conjunto: UIPickerView!
    @IBOutlet weak var apto: UITextField!
    
//    var conjuntos : [String] = ["Calleja","Torres de la colina"]
    var conjuntos : [String] = []

    var ref: DatabaseReference!
    
    var handle: AuthStateDidChangeListenerHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertaInternet()
        ref = Database.database().reference()
        ref?.child("Conjuntos").observe(.childAdded, with: { (snapshot) in
           // let postDict = snapshot.value as? [String : AnyObject] ?? [:]
            let con = snapshot.key
            self.conjuntos.append(con)
            self.conjunto.dataSource = self
            self.conjunto.delegate = self
        })
        
      
    
        
        
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return conjuntos.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return conjuntos[row]
    }
    
    
    
    @IBAction func registrarse(_ sender: Any) {
        guard let username = a.text else { return }
        guard let email = B.text else { return }
        guard let password = C.text else { return }
        guard let passconfir = D.text else { return }
        guard let iddoc = doc.text else { return }
        guard let nom = nombres.text else { return }
        guard let ape = apellidos.text else { return }
        guard let aparta = apto.text else { return }
        let conjunteishion = conjunto.selectedRow(inComponent: 0)
        //var puede = false
        
        // Verificar campos en blanco
        
        if((username.isEmpty) || (email.isEmpty) || (password.isEmpty) || (passconfir.isEmpty) || (iddoc.isEmpty) || (nom.isEmpty) || (ape.isEmpty) || (aparta.isEmpty)){
            
            displayAlertMessage(message: "Todos los campos son requeridos");
            return;
        }
        if( (username.contains(".")) || (username.contains("#")) || (username.contains("$")) || (username.contains("[")) || (username.contains("]")) ){
            displayAlertMessage(message: "El nombre de usuario no puede contener los siguientes caracteres '.' '#' '$' '[' or ']'");
            return;
            
        }
        // Verificar largo de los textos
        if ((username.count) < 2 || (username.count) > 30){
            displayAlertMessage(message: "El nombre de usuario debe tener entre 8 y 30 caracteres");
            return;
        }
        
        if isValidEmail(testStr: email){
            if (email.count) > 50{
                displayAlertMessage(message: "El correo electronico no puede tener mas de 50 caracteres");
                return;
            }
        }else{
            displayAlertMessage(message: "Correo electronico invalido");
            return;
        }
        
        if ((password.count) < 8 || (password.count) > 30 ){
            displayAlertMessage(message: "La contraseña debe tener entre 8 y 30 caracteres");
            // Ver si las contra son iguales
            
            if(password != passconfir){
                //display alert
                displayAlertMessage(message: "Las contraseñas no coinciden");
                return;
            }
            return;
        }
        
        if ((iddoc.count) < 8 || (iddoc.count) > 30){
            displayAlertMessage(message: "La cedula debe tener entre 8 y 30 caracteres");
            return;
        }
        if ((nom.count) < 3 || (nom.count) > 50){
            displayAlertMessage(message: "Los nombres deben tener entre 3 y 30 caracteres");
            return;
        }
        if ((ape.count) < 3 || (ape.count) > 50){
            displayAlertMessage(message: "Los apellidos deben tener entre 3 y 30 caracteres");
            return;
        }
        // Verificar largo de los textos
        if ((aparta.count) < 1 || (aparta.count) > 5){
            displayAlertMessage(message: "EL apartamento debe tener entre 1 y 5 caracteres");
            return;
        }
        
        
        // Almacenar datos
        let fireEmail =  email.replacingOccurrences(of: ".", with: ",", options: .literal, range: nil)
        
        /*Crear Usuario firebase
         Auth.auth().createUser(withEmail: email, password: password) { user, error in
         if error == nil && user != nil {
         print("User created!")
         
         let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
         changeRequest?.displayName = username
         
         changeRequest?.commitChanges { error in
         if error == nil {
         print("User display name changed!")
         self.dismiss(animated: false, completion: nil)
         } else {
         print("Error: \(error!.localizedDescription)")
         }
         }
         
         } else {
         print("Error: \(error!.localizedDescription)")
         }
         }
         */
        
        
        // DATABASE CONECTION
        var ref: DatabaseReference!
        ref = Database.database().reference()
        let sin = "Sin actualizar"
        //let apellidoa = sin
        let aptoa = aparta
        //let cedulaa = sin
        let conjuntoa = conjuntos[conjunteishion]
        let nicknamekey = username.lowercased()
        
        
        
        ref.child("Perfiles").child(nicknamekey).setValue(["nombre": nom , "email":fireEmail, "pass":password, "apellido": ape, "apto":aptoa, "cedula":iddoc, "conjunto":conjuntoa])
        ref.child("Perfiles").child(nicknamekey).child("vehiculo").setValue(["marca": sin,"modelo": sin ,"placa": sin])
        
        
        
        // Mostrar mensaje con la confirmacion
        let confirm = UIAlertController(title: "Exito", message: "Su registro esta completo", preferredStyle: .alert)
        confirm.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            self.dismiss(animated: true)
        }))
        self.present(confirm, animated: true, completion: nil)
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            // [START_EXCLUDE]
           // self.setTitleDisplay(user)
            //self.tableView.reloadData()
            // [END_EXCLUDE]
        
    }
    /*
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // [START remove_auth_listener]
        Auth.auth().removeStateDidChangeListener(handle!)
        // [END remove_auth_listener]
    }
*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    public func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func botonRegistroPress(_ sender: Any) {
        
       
    }
    
    func displayAlertMessage(message:String){
        let alert = UIAlertController(title: "Alerta", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }

    
    func hayInternet() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var Flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &Flags) {
            return false
        }
        let puede = Flags.contains(.reachable)
        let necesita = Flags.contains(.connectionRequired)
        return (puede && !necesita)
    }
    
    func alertaInternet() {
        if !hayInternet() {
            let alerta = UIAlertController(title: "Precaución", message: "En el momento usted se encuentra sin conexión, las acciones que ejecute enn este estado en nuestra aplicacion pueden que no se ejecuten, muchas gracias por su comprensión.", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alerta.addAction(action)
            present(alerta, animated: true, completion: nil)
        }
    }

}
