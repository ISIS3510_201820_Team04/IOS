//
//  SliderPersonalizado.swift
//  ReRe
//
//  Created by Julian Camilo on 9/17/18.
//  Copyright © 2018 Julian Camilo. All rights reserved.
//

import UIKit

@IBDesignable
class SliderPersonalizado: UISlider {

    @IBInspectable var thumbImage: UIImage?{
        didSet {
            setThumbImage(thumbImage, for: .normal)
            
        }
    }
   
    
    @IBInspectable var thumbResaltado: UIImage?{
        didSet{
            setThumbImage(thumbImage, for: .highlighted)
        }
    }

}
